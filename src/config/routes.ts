import APP_CONST from './constants';

export default {
  Root: APP_CONST.EMPTY_LINE,
  Survey: 'survey',
  Shop: 'shop',
  Checkout: 'checkout',
  UserInfo: 'user-info',
  PurchaseSuccess: 'purchase-success',
  Basket: 'basket',
  Login: 'login',
  Admin: 'admin',
}
