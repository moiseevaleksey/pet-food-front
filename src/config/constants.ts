const APP_CONST = {
  CURRENCY: {
    EUR: '€',
    USD: '$',
    UAH: '₴',
  },
  EMPTY_LINE: '',
  DEBOUNCE_TIME: 400,
  STORAGE_KEY: {
    AUTH_DATA: 'Pf-Auth-Data'
  }
};

export default APP_CONST;
