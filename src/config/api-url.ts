// const HOST = 'http://185.25.116.144';
export const HOST = 'https://mealbox.com.ua:8095';

export const ApiUrl = {
  anonToken: `${ HOST }/api/initialUser/create`,
  catalogs: `${ HOST }/anon/questions/catalogs/all`,
  checkAnonToken: `${ HOST }/anon/testCallAnonOnly`,
  recommendedFood: `${ HOST }/anon/meal/recommend`,
  saveOrder: `${ HOST }/anon/checkout/placeOrder`,
  createAnimal: `${ HOST }/anon/animal/create`,
  itemsByPetCategory: `${ HOST }/anon/items/get`, // without price
  catalogByPetCategory: `${ HOST }/anon/catalog/get`, // with price
  regionList: `${ HOST }/api/regions`,
  orderDetails: `${ HOST }/anon/order`,
  shopItemDetails: `${ HOST }/anon/catalog/item`,
  registration: `${ HOST }/api/registration`,
  checkOut: `${ HOST }/anon/checkout/delivery_address`,
}

