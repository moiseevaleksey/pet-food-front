import { Component, OnInit } from '@angular/core';
import { AuthService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'pf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public isLoaded$: Observable<boolean> = new Observable<boolean>();

  public constructor(
    private readonly auth: AuthService,
    private readonly translate: TranslateService,
  ) {
  }
  public ngOnInit(): void {
    this.isLoaded$ = this.auth.getAuthDataAsObs().pipe(
      map(Boolean),
    );

    this.auth.initAuthData();
    this.translate.addLangs(['en', 'ua']);
    this.translate.setDefaultLang('ua');

    // const browserLang = this.translate.getBrowserLang();
    this.translate.use('ua');
    // this.translate.use(browserLang.match(/en|fr/) ? browserLang : 'fr');
  }
}
