export interface DeliveryRegionModel {
  AreasCenter: string;
  Description: string;
  DescriptionRu: string;
  Ref: string;
}

export interface DeliveryCityModel {
  Area: string;
  AreaDescription: string;
  AreaDescriptionRu: string;
  CityID: string;
  Description: string;
  DescriptionRu: string;
  IsBranch: string;
  PreventEntryNewStreetsUser: string;
  Ref: string;
  SettlementType: string;
  SettlementTypeDescription: string;
  SettlementTypeDescriptionRu: string;
  SpecialCashCheck: string;
}

export interface DeliveryPostOfficeModel {
  SiteKey: string;
  Description: string;
  DescriptionRu: string;
  ShortAddress: string;
  ShortAddressRu: string;
  Phone: string;
  TypeOfWarehouse: string;
  Ref: string;
  Number: string;
  CityRef: string;
  CityDescription: string;
  CityDescriptionRu: string;
  SettlementRef: string;
  SettlementDescription: string;
  SettlementAreaDescription: string;
  SettlementRegionsDescription: string;
  SettlementTypeDescription: string;
  SettlementTypeDescriptionRu: string;
  Longitude: string;
  Latitude: string;
  PostFinance: string;
  BicycleParking: string;
  PaymentAccess: string;
  POSTerminal: string;
  InternationalShipping: string;
  SelfServiceWorkplacesCount: string;
  TotalMaxWeightAllowed: string;
  PlaceMaxWeightAllowed: string;
  DistrictCode: string;
  WarehouseStatus: string;
  Direct: string;
  RegionCity: string;
}
