import { DeliveryCityModel, DeliveryPostOfficeModel, DeliveryRegionModel } from 'src/app/core/models/delivery';

export interface PetModel {
  age: number;
  animalCategory: { id: number; name: string; }
  id: number;
  name: string;
  size: DogSizeEnum;
}

export interface SkuItemModel {
  id: number;
  originCountry: string;
  manufacturer: string;
  brand: string;
  skuName: string;
  taste: string;
  description: string;
  animalCategory: AnimalCategoryModel,
  animalSubCategories: [],
  petAgeGroups: PetAgeGroupModel[],
  foodType: FoodTypeModel,
  foodSubType: any;
  packageWeightKilos: number;
  packageVolumeLiter: number;
  animalSizes: AnimalSizeModel[],
  smallImgPath: string;
  largeImgPath: string;
  imgName: string;
}

export interface AnimalCategoryModel {
  id: number;
  name: string;
}

export interface PetAgeGroupModel {
  id: number;
  petAgeRange: string;
}

export interface FoodTypeModel {
  code: number
  id: number;
  type: string;
}

export interface AnimalSizeModel {
  id: number;
  shortName: DogSizeEnum
  longName: string;
}

export interface FoodSubTypeModel {
  id: number;
  subType: string;
}

export interface ShopItemDataModel {
  id: number;
  skuItem: SkuItemModel,
  vendor: VendorModel,
  basePrice: number;
  sellingPrice: number;
}

export interface VendorModel {
  id: number;
  title: string;
}

export interface AuthDataModel {
  email: string;
  token: string;
}

export interface UserInfoModel {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  region: DeliveryRegionModel;
  city: DeliveryCityModel;
  postOffice: DeliveryPostOfficeModel;
}

export interface PetCategoryModel {
  id: number;
  value: PetTypeEnum;
  label: string;
  main: boolean;
}

export interface ProductModel {
  animalCategory: { id: number; name: string };
  brand: string;
  description: string;
  id: number;
  imgName: string;
  largeImgPath: string;
  manufacturer: null;
  originCountry: null;
  packageWeightKilos: number;
  skuName: string;
  smallImgPath: string;
  taste: string;
}

export interface BasketItemModel<T> {
  id: string;
  label: string;
  description?: string;
  price: number;
  item: T;
}

export enum PetTypeEnum {
  Cat = 'Cat',
  Dog = 'Dog',
  Fish = 'Fish',
  Reptile = 'Reptile',
  Bird = 'Bird',
  Rodent = 'Rodent',
}

export enum DogSizeEnum {
  ExtraSmall = 'XS',
  Small = 'S',
  Avg = 'X',
  Big = 'XL',
}

export enum FoodTypeEnum {
  Wet = 1,
  Dry = 2,
  Mixed = 3,
}

export enum PurchaseFrequencyEnum {
  Often = 'Often',
  Rare = 'Rare',
}
