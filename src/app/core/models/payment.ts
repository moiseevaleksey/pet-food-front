import { PetModel, SkuItemModel } from 'src/app/core/models/core';

export interface SubscriptionPaymentRequestModel {
  public_key: string;
  version: string;
  result_url: string;
  server_url: string;
  action: PaymentActionEnum;
  amount: string;
  currency: PaymentCurrencyEnum;
  description: string;
  order_id: string;
  subscribe_periodicity: PaymentFrequencyEnum;
  subscribe: string;
  subscribe_date_start: string;
}

export interface SinglePaymentRequestModel {
  order_id: string;
  public_key: string;
  version: string;
  result_url: string;
  server_url: string;
  action: PaymentActionEnum;
  amount: string;
  currency: PaymentCurrencyEnum;
  description: string;
}

export interface OrderModel {
  animal: PetModel;
  deliveryAddress: string;
  orderAmount: number;
  order_id: string;
  user: OrderUserModel;
  orderItems: OrderItemModel[];
  orderCreationDate: string;
  orderPaymentDate: any;
  subscription: boolean;
  subscriptionStatus: any;
  paymentStatus: string;
}

export interface OrderItemModel {
  id: number;
  quantity: number;
  skuItem: SkuItemModel;
}

export interface OrderUserModel {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  newUserFlag: boolean;
  username: string;
}

export enum PaymentCurrencyEnum {
  USD = 'USD',
  EUR = 'EUR',
  RUB = 'RYB',
  UAH = 'UAH',
  BYN = 'BYN',
  KZT = 'KZT',
}

export enum PaymentActionEnum {
  Pay = 'pay',
  Hold = 'hold',
  Subscribe = 'subscribe',
  Paydonate = 'paydonate',
  Auth = 'auth',
}


export enum PaymentFrequencyEnum {
  Month = 'month',
  Year = 'year',
}
