export interface RegisterUserModel {
  username: string;
  password: string;
}

export interface RegisterUserSuccessModel {
  created: string;
  email: string;
  status: string;
}
