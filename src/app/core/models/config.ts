import {
  DogSizeEnum,
  FoodTypeEnum,
  PetTypeEnum,
  PetCategoryModel,
  PurchaseFrequencyEnum,
} from 'src/app/core/models/core';
import { SurveyOptionModel } from 'src/app/feature/survey/models/survey.model';

export const animalCategories: PetCategoryModel[] = [
  {
    id: 1,
    value: PetTypeEnum.Dog,
    label: 'feature.survey.pet-type.dog',
    main: true,
  },
  {
    id: 2,
    value: PetTypeEnum.Cat,
    label: 'feature.survey.pet-type.cat',
    main: true,
  },
  {
    id: 3,
    value: PetTypeEnum.Fish,
    label: 'feature.survey.pet-type.fish',
    main: false,
  },
  {
    id: 4,
    value: PetTypeEnum.Rodent,
    label: 'feature.survey.pet-type.rodent',
    main: false,
  },
  {
    id: 5,
    value: PetTypeEnum.Reptile,
    label: 'feature.survey.pet-type.reptile',
    main: false,
  },
  {
    id: 6,
    value: PetTypeEnum.Bird,
    label: 'feature.survey.pet-type.bird',
    main: false,
  },
];

export const dogSize: SurveyOptionModel<DogSizeEnum>[] = [
  { id: 1, label: 'feature.survey.dog-size-step.XS', value: DogSizeEnum.ExtraSmall, ico: 'assets/pic/extra-small-dog.svg' },
  { id: 2, label: 'feature.survey.dog-size-step.S', value: DogSizeEnum.Small, ico: 'assets/pic/small-dog.svg' },
  { id: 3, label: 'feature.survey.dog-size-step.X', value: DogSizeEnum.Avg, ico: 'assets/pic/avg-dog.svg' },
  { id: 4, label: 'feature.survey.dog-size-step.XL', value: DogSizeEnum.Big, ico: 'assets/pic/big-dog.svg' },
];

export const frequencyOptions: SurveyOptionModel<PurchaseFrequencyEnum>[] = [
  { id: 1, label: 'often', value: PurchaseFrequencyEnum.Often },
  { id: 2 ,label: 'rare', value: PurchaseFrequencyEnum.Rare },
];

export const FoodTypeOptions: SurveyOptionModel<FoodTypeEnum>[] = [
  { id: 1, value: FoodTypeEnum.Dry, label: 'Dry'},
  { id: 2, value: FoodTypeEnum.Wet, label: 'Wet'},
  { id: 3, value: FoodTypeEnum.Mixed, label: 'Mixed'},
];
