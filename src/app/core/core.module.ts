import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from 'src/app/core/interceptors';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const CORE_MODULES = [
  ReactiveFormsModule,
  HttpClientModule,
  TranslateModule,
  CommonModule,
];

@NgModule({
  declarations: [],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  imports: [
    ...CORE_MODULES,
  ],
  exports: [
    ...CORE_MODULES,
  ],
})
export class CoreModule {
}
