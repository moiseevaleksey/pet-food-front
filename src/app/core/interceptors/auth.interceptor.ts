import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders, HttpErrorResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core/services';
import { AuthDataModel } from 'src/app/core/models';
import { tap } from 'rxjs/operators';
import APP_CONST from 'src/config/constants';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private readonly authService: AuthService,
  ) {
  }

  public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authData = this.authService.getAuthData();
    const isAuthRequired = !request.headers.has('no-auth');

    const storedAuthData = this.authService.readAuthData();
    const token = authData ? authData.token : storedAuthData ? storedAuthData.token : APP_CONST.EMPTY_LINE;
    const headers = isAuthRequired
      ? new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer_${ token }`)
      : new HttpHeaders()
        .set('Content-Type', 'application/json');
    // isAuthRequired && headers;

    const newRequest = request.clone({ headers });

    return next.handle(newRequest)
      .pipe(
        tap(() => {
          if (!this.authService.getAuthData()) {
            this.authService.storeAuthData(storedAuthData);
          }
        }, (err: Error) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 403) {
              this.authService.getAnonToken()
                .subscribe((data: AuthDataModel) => this.authService.storeAuthData(data));
            }
          }
        }),
      );
  }
}
