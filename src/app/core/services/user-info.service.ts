import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterUserModel, RegisterUserSuccessModel } from 'src/app/core/models/register-user.model';
import { ApiService } from 'src/app/core/services/api.service';
import { ApiUrl } from 'src/config/api-url';
import { AuthDataModel, UserInfoModel } from 'src/app/core/models';

export interface UserCheckOutModel {
  orderId: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  regionRef: string;
  region: string;
  cityRef: string;
  city: string;
  branchRef: string;
  branch: string;
  description: string;
}

@Injectable({
  providedIn: 'root',
})
export class UserInfoService {
  // private user: BehaviorSubject<UserInfoModel | null> = new BehaviorSubject<UserInfoModel | null>(null);

  constructor(
    private readonly apiService: ApiService,
  ) {
  }

  // public setUser(user: UserInfoModel): void {
  //   this.user.next(user);
  // }

  public sendUserDataWithOrder(user: UserInfoModel, orderId: string): Observable<AuthDataModel> {
    return this.postUserDataWithOrder(UserInfoService.mapCheckoutData(user, orderId));
  }

  private postUserDataWithOrder(userCheckoutData: UserCheckOutModel): Observable<AuthDataModel> {
    return this.apiService.post(ApiUrl.checkOut, userCheckoutData);
  }

  public registerNewUser(user: RegisterUserModel): Observable<RegisterUserSuccessModel> {
    return this.apiService.post(ApiUrl.registration, user);
  }

  private static mapCheckoutData(userInfo: UserInfoModel, orderId: string): UserCheckOutModel {
    return {
      orderId,
      firstName: userInfo.firstName,
      lastName: userInfo.lastName,
      email: userInfo.email,
      phoneNumber: userInfo.phone,
      region: userInfo.region.Description,
      regionRef: userInfo.region.Ref,
      city: userInfo.city.Description,
      cityRef: userInfo.city.Ref,
      branch: userInfo.postOffice.Description,
      branchRef: userInfo.postOffice.Description,
      description: 'Description?',
    };
  }
}
