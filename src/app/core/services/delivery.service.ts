import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DeliveryCityModel, DeliveryPostOfficeModel, DeliveryRegionModel } from 'src/app/core/models/delivery';
import { HttpHeaders } from '@angular/common/http';

const newPostUrl = 'https://api.novaposhta.ua/v2.0/json/';
const API_KEY = 'a026258501de971b939e2c930de64d1f';
const NO_AUTH = new HttpHeaders({ 'no-auth': 'true' });

@Injectable({
  providedIn: 'root',
})
export class DeliveryService {
  constructor(
    private readonly apiService: ApiService,
  ) {
  }

  public getRegions(): Observable<DeliveryRegionModel[]> {
    return this.apiService.post(
      newPostUrl,
      {
        apiKey: API_KEY,
        modelName: 'Address',
        calledMethod: 'getAreas',
        methodProperties: {},
      },
      NO_AUTH,
    ).pipe(
      map((data: any) => data.data),
    )
  }

  public getCities(areaRef: string): Observable<DeliveryCityModel[]> {
    return this.apiService.post(
      newPostUrl,
      {
        apiKey: API_KEY,
        modelName: 'Address',
        calledMethod: 'getCities',
        methodProperties: { AreaRef: areaRef },
      },
      NO_AUTH,
    ).pipe(
      map((data: any) => data.data),
    )
  }

  public getPostOffices(cityRef: string): Observable<DeliveryPostOfficeModel[]> {
    return this.apiService.post(
      newPostUrl,
      {
        apiKey: API_KEY,
        modelName: 'AddressGeneral',
        calledMethod: 'getWarehouses',
        methodProperties: { CityRef: cityRef },
      },
      NO_AUTH,
    ).pipe(
      map((data: any) => data.data),
    )
  }


}
