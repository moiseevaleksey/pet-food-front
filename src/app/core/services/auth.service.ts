import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import APP_CONST from 'src/config/constants';
import { ApiService } from 'src/app/core/services';
import { ApiUrl } from 'src/config/api-url';
import { AuthDataModel } from 'src/app/core/models/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private authData: BehaviorSubject<AuthDataModel | null> = new BehaviorSubject<AuthDataModel | null>(null);
  private isAuth: boolean = false;
  private redirectUrl: string = APP_CONST.EMPTY_LINE;

  constructor(
    private readonly api: ApiService,
  ) {
  }

  public initAuthData(): void {
    this.checkToken().subscribe(() => {
    });
  }

  public getAnonToken(): Observable<AuthDataModel> {
    return this.api.post<AuthDataModel>(ApiUrl.anonToken);
  }

  public getAuthData(): AuthDataModel | null {
    return this.authData.getValue();
  }

  public getAuthDataAsObs(): Observable<AuthDataModel | null> {
    return this.authData.asObservable();
  }

  public readAuthData(): AuthDataModel {
    const data = localStorage.getItem(APP_CONST.STORAGE_KEY.AUTH_DATA);
    return data ? JSON.parse(data) : null;
  }

  public storeAuthData(data: AuthDataModel): void {
    localStorage.setItem(APP_CONST.STORAGE_KEY.AUTH_DATA, JSON.stringify(data));
    this.authData.next(data);
  }

  public setIsAuthenticated(value: boolean): void {
    this.isAuth = value;
  }

  public isAuthenticated(): boolean {
    return this.isAuth;
  }

  public saveRedirectUrl(url: string): void {
    this.redirectUrl = url;
  }

  public getRedirectUrl(): string {
    return this.redirectUrl;
  }

  private checkToken(): Observable<any> {
    return this.api.get(ApiUrl.checkAnonToken);
  }
}
