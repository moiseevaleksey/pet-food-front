import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BasketItemModel, ShopItemDataModel } from 'src/app/core/models/core';
import uniq from 'uniqid';
import APP_CONST from 'src/config/constants';
import { MealBoxModel } from 'src/app/feature/survey/models/survey.model';

export type BasketItemType = MealBoxModel | ShopItemDataModel;
export type BasketType = BasketItemModel<BasketItemType>;

@Injectable({
  providedIn: 'root',
})
export class BasketService {
  private basket$: BehaviorSubject<BasketType[]> = new BehaviorSubject<BasketType[]>([]);

  constructor() {
    this.basket$.subscribe(console.log);
  }

  public addToBasket(basketItem: BasketItemType): void {
    const mappedBasketItem = BasketService.mapBasketItem(basketItem);

    this.basket$.next([...this.basket$.getValue(), mappedBasketItem]);
  }

  public removeFromBasket(basketItem: BasketType): void {
    const state = this.basket$.getValue();
    const newState = state.filter((item: BasketType) => item.id !== basketItem.id);

    this.basket$.next(newState);
  }

  public getBasket(): BehaviorSubject<BasketType[]> {
    return this.basket$;
  }

  private static mapBasketItem(basketItem: BasketItemType): BasketItemModel<BasketItemType> {
    // const description = meal.details
    //   .map(({ taste, foodType, itemCount, itemWeight }) => {
    //     return `${ taste } ${ foodType } ${ itemCount }x${ itemWeight }`;
    //   })
    //   .join(', ');
    return {
      id: uniq(),
      item: basketItem,
      price: 0,
      label: APP_CONST.EMPTY_LINE,
      description: APP_CONST.EMPTY_LINE,
    };
  }
}
