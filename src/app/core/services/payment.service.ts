import { Injectable } from '@angular/core';
import {
  OrderModel,
  PaymentActionEnum,
  PaymentCurrencyEnum,
  PaymentFrequencyEnum,
  SinglePaymentRequestModel,
  SubscriptionPaymentRequestModel,
} from 'src/app/core/models';
import APP_CONST from 'src/config/constants';
import { encode as base64encode } from 'base-64';
import sha1 from 'sha1';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from 'src/app/core/services/api.service';
import { ApiUrl } from 'src/config/api-url';

const version = '3'; // post office api version
const public_key = 'sandbox_i95284259315';
const server_url = 'https://mealbox.com.ua:8095/api/liqpay/payment/result';
const privateKey = 'sandbox_a3fKl4MFuwBEptNyt4IVRGWPz46lu9HIZa1ihX5m';

export interface OrderModel1 {
  animalId: number;
  orderId: string;
  orderTotalAmount: number;
  userId: number;
}

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  private readonly order$: BehaviorSubject<OrderModel1 | null> = new BehaviorSubject<OrderModel1 | null>(null);

  constructor(
    private readonly apiService: ApiService,
  ) {
    this.order$.subscribe(console.log);
  }

  public getSubscriptionPaymentRequest(
    order: OrderModel,
    description: string = APP_CONST.EMPTY_LINE,
    currency: PaymentCurrencyEnum = PaymentCurrencyEnum.UAH,
    action: PaymentActionEnum = PaymentActionEnum.Pay,
  ): SubscriptionPaymentRequestModel {
    const order_id = order.order_id;

    return {
      public_key,
      version,
      result_url: PaymentService.getResultUrl(order_id),
      server_url,
      amount: String(order.orderAmount),
      currency,
      description,
      action,
      subscribe: '0',
      subscribe_date_start: '2022-11-16 08:00:00', // TODO: recheck
      subscribe_periodicity: PaymentFrequencyEnum.Month,
      order_id,
    };
  }

  public getSinglePaymentRequest(
    order: OrderModel,
    description: string,
  ): SinglePaymentRequestModel {
    const order_id = order.order_id;
    return {
      order_id,
      public_key,
      version,
      result_url: PaymentService.getResultUrl(order_id),
      server_url,
      action: PaymentActionEnum.Pay,
      amount: String(order.orderAmount),
      currency: PaymentCurrencyEnum.UAH,
      description,
    };
  }


  public setOrder(order: OrderModel1): void {
    this.order$.next(order);
  }

  public getOrder(): Observable<OrderModel1 | null> {
    return this.order$.asObservable();
  }

  public getOrderAsValue(): OrderModel1 | null {
    return this.order$.getValue();
  }

  public encodeBase64(str: string): string {
    return base64encode(str);
  }

  public getSignString(str: string): string {
    return privateKey + str + privateKey;
  }

  public getSignature(signString: string): string {
    return this.hexToBase64(sha1(signString));
  }

  public getOrderDetails(orderId: string): Observable<OrderModel> {
    return this.apiService.get<OrderModel>(ApiUrl.orderDetails + '/' + orderId);
  }

  private hexToBase64(hexstring: string): string {
    return btoa(hexstring.match(/\w{2}/g)!.map(function (a) {
      return String.fromCharCode(parseInt(a, 16));
    }).join(''));
  }

  private static getResultUrl(orderId: string): string {
    const resultUrl = new URL('http://localhost:4200/purchase-success');
    resultUrl.searchParams.append('orderId', orderId);

    return resultUrl.href;
  }

}
