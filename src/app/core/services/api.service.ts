import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(
    private readonly http: HttpClient,
  ) {
  }

  public get<T>(url: string, params?: HttpParams): Observable<T> {
    return this.http.get<T>(url, { params });
  }

  public post<T>(url: string, body?: object, headers?: HttpHeaders): Observable<T> {
    return this.http.post<T>(url, body, { headers });
  }
}
