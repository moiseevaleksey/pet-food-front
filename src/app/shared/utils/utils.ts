export const getAgeInMonth = (years: number, month: number): number => {
  const MONTH_IN_YEAR = 12;
  return MONTH_IN_YEAR * years + month;
}
