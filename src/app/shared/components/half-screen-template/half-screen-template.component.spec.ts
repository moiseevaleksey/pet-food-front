import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HalfScreenTemplateComponent } from 'src/app/shared/components/half-screen-template/half-screen-template.component';

describe('GenericSurveyStepComponent', () => {
  let component: HalfScreenTemplateComponent;
  let fixture: ComponentFixture<HalfScreenTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HalfScreenTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HalfScreenTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
