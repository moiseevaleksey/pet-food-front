import { Component } from '@angular/core';

@Component({
  selector: 'pf-half-screen-template',
  templateUrl: './half-screen-template.component.html',
  styleUrls: ['./half-screen-template.component.scss']
})
export class HalfScreenTemplateComponent {
    constructor() { }
}
