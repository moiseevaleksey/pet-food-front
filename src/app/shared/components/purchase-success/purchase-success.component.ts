import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import APP_CONST from 'src/config/constants';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderModel } from 'src/app/core/models';
import { PaymentService, UserInfoService } from 'src/app/core/services';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'pf-purchase-success',
  templateUrl: './purchase-success.component.html',
  styleUrls: ['./purchase-success.component.scss'],
})
export class PurchaseSuccessComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public order$: Observable<OrderModel> = new Observable<OrderModel>();
  public order: OrderModel | null = null;
  private orderId: string | null = null;

  constructor(
    private readonly fb: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly paymentService: PaymentService,
    private readonly userInfoService: UserInfoService,
    private readonly router: Router,
  ) {
  }

  public ngOnInit(): void {
    this.orderId = this.activatedRoute.snapshot.queryParamMap.get('orderId');
    if (this.orderId === null) {
      console.error('No order id!');
    } else {
      this.order$ = this.paymentService.getOrderDetails(this.orderId)
        .pipe(
          tap((order: OrderModel) => this.order = order),
        );
    }

    this.form = this.fb.group(
      {
        password: [APP_CONST.EMPTY_LINE, Validators.compose(([Validators.required, Validators.min(6)]))],
        confirmPassword: [APP_CONST.EMPTY_LINE],
      },
      { validators: this.checkPasswords },
    );
  }

  public checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    const pass = group.get('password')?.value;
    const confirmPass = group.get('confirmPassword')?.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  public createAccount(): void {
    const { password } = this.form.getRawValue();
    this.userInfoService.registerNewUser({ username: this.order?.user?.email!, password })
      .subscribe(() => this.router.navigate([ '/']));
  }
}
