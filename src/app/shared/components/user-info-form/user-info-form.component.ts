import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaymentService, UserInfoService } from 'src/app/core/services';
import RoutePath from 'src/config/routes';
import { Router } from '@angular/router';
import { DeliveryService } from 'src/app/core/services/delivery.service';
import { from, Observable } from 'rxjs';
import { DeliveryCityModel, DeliveryPostOfficeModel, DeliveryRegionModel, UserInfoModel } from 'src/app/core/models';
import { switchMap, tap } from 'rxjs/operators';
import APP_CONST from 'src/config/constants';

enum AddressFieldsEnum {
  Region = 'region',
  City = 'city',
  PostOffice = 'postOffice',
}

const NO_EVENT_OPTS = { emitEvent: false };

@Component({
  selector: 'pf-user-info-form',
  templateUrl: './user-info-form.component.html',
  styleUrls: ['./user-info-form.component.scss'],
})
export class UserInfoFormComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public regions$: Observable<DeliveryRegionModel[]> = new Observable<DeliveryRegionModel[]>();
  public cities$: Observable<DeliveryCityModel[]> | null = null;
  public postOffices$: Observable<DeliveryPostOfficeModel[]> | null = null;

  constructor(
    private readonly fb: FormBuilder,
    private readonly userInfoService: UserInfoService,
    private readonly router: Router,
    private readonly deliveryService: DeliveryService,
    private readonly paymentService: PaymentService,
  ) {
  }

  public ngOnInit(): void {
    this.form = this.fb.group({
      firstName: ['Alex', Validators.required],
      lastName: ['M', Validators.required],
      phone: ['+123', Validators.required],
      email: ['asd@qw.qw', Validators.compose([Validators.email, Validators.required])],
      [AddressFieldsEnum.Region]: [{ value: APP_CONST.EMPTY_LINE, disabled: true }, Validators.required],
      [AddressFieldsEnum.City]: [{ value: APP_CONST.EMPTY_LINE, disabled: true }, Validators.required],
      [AddressFieldsEnum.PostOffice]: [{ value: APP_CONST.EMPTY_LINE, disabled: true }, Validators.required],
    });

    this.regions$ = this.deliveryService.getRegions()
      .pipe(
        tap(() => this.form.controls[AddressFieldsEnum.Region].enable()),
      );

    this.form.controls[AddressFieldsEnum.Region].valueChanges
      .subscribe((region: DeliveryRegionModel) => {

        this.form.controls[AddressFieldsEnum.City].enable(NO_EVENT_OPTS);
        this.form.controls[AddressFieldsEnum.City].reset(null, NO_EVENT_OPTS);
        this.form.controls[AddressFieldsEnum.PostOffice].reset(null, NO_EVENT_OPTS);
        this.form.controls[AddressFieldsEnum.PostOffice].disable(NO_EVENT_OPTS);

        if (region.Ref) {
          this.cities$ = this.deliveryService.getCities(region.Ref);
        }
      });

    this.form.controls[AddressFieldsEnum.City].valueChanges.subscribe((city: DeliveryCityModel) => {
      this.form.controls[AddressFieldsEnum.PostOffice].enable(NO_EVENT_OPTS);
      this.form.controls[AddressFieldsEnum.PostOffice].reset(null, NO_EVENT_OPTS);
      if (city.Ref) {
        this.postOffices$ = this.deliveryService.getPostOffices(city.Ref);
      }
    });
  }

  public setUser(): void {
    const order = this.paymentService.getOrderAsValue();

    if (order) {
      const user: UserInfoModel = this.form.getRawValue();
      this.userInfoService.sendUserDataWithOrder(user, order.orderId)
        .pipe(
          switchMap(() => from(this.router.navigate([RoutePath.Checkout], { queryParams: { orderId: order?.orderId } }))),
        )
        .subscribe(() => {});
    }
  }
}
