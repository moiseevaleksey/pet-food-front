import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import APP_CONST from 'src/config/constants';
import { AuthService } from 'src/app/core/services';
import { Router } from '@angular/router';

@Component({
  selector: 'pf-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  public form: FormGroup = new FormGroup({});

  constructor(
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
  ) {
  }

  public ngOnInit(): void {
    this.form = this.fb.group({
      name: APP_CONST.EMPTY_LINE,
      password: APP_CONST.EMPTY_LINE,
    });
  }

  public login(): void {
    this.authService.setIsAuthenticated(true);
    this.router.navigate([this.authService.getRedirectUrl()]);
  }
}
