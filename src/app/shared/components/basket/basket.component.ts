import { Component, OnInit } from '@angular/core';
import { BasketService, BasketType } from 'src/app/core/services';
import { BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'pf-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss'],
})
export class BasketComponent implements OnInit {
  public basket$: BehaviorSubject<BasketType[]> = new BehaviorSubject<BasketType[]>([]);

  constructor(
    private readonly basketService: BasketService,
    private readonly location: Location,
  ) {
  }

  public ngOnInit(): void {
    this.basket$ = this.basketService.getBasket();
  }

  public removeFromBasket(basketItem: BasketType): void {
    this.basketService.removeFromBasket(basketItem);
  }

  public back(): void {
    this.location.back();
  }
}
