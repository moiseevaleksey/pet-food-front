import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/ui-kits/material.module';
import {
  BasketComponent,
  FooterComponent,
  HalfScreenTemplateComponent,
  LoaderComponent,
  LoginFormComponent,
  PurchaseSuccessComponent,
  TopToolbarComponent,
  UserInfoFormComponent,
} from 'src/app/shared/components';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'src/app/core/core.module';

const SHARED_COMPONENTS = [
  BasketComponent,
  FooterComponent,
  HalfScreenTemplateComponent,
  LoaderComponent,
  LoginFormComponent,
  PurchaseSuccessComponent,
  TopToolbarComponent,
  UserInfoFormComponent,
];

@NgModule({
  declarations: [
    ...SHARED_COMPONENTS,
    FooterComponent,
  ],
  imports: [
    CoreModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [
    MaterialModule,
    RouterModule,
    ...SHARED_COMPONENTS,
  ],
})
export class SharedModule {
}
