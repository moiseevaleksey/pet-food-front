import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CheckoutModule, HomeModule, ShopModule, SurveyModule } from './feature';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

const FEATURE_MODULES = [
  CheckoutModule,
  HomeModule,
  ShopModule,
  SurveyModule,
];

function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),

    ...FEATURE_MODULES,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
