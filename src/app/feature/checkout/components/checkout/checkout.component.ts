import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BasketService, PaymentService } from 'src/app/core/services';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderItemModel, OrderModel, PaymentActionEnum, PaymentCurrencyEnum } from 'src/app/core/models';
import APP_CONST from 'src/config/constants';
import RoutePath from 'src/config/routes';

@Component({
  selector: 'pf-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public order$: Observable<OrderModel> = new Observable<OrderModel>();
  public orderItems: OrderItemModel[] = [];
  public paymentData: string = APP_CONST.EMPTY_LINE;
  public signature: string = APP_CONST.EMPTY_LINE;
  public paymentTypeCtrl: FormControl = new FormControl();
  public readonly uahSign = APP_CONST.CURRENCY.UAH;
  private order: OrderModel | null = null;

  constructor(
    private readonly basketService: BasketService,
    private readonly paymentService: PaymentService,
    private readonly fb: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) {
  }

  public ngOnInit(): void {
    const orderId = this.activatedRoute.snapshot.queryParamMap.get('orderId');
    if (!orderId) {
      this.handleLoadOrderError();
      return;
    }

    this.paymentTypeCtrl = this.fb.control(false);
    this.paymentTypeCtrl.valueChanges.subscribe((isSingleOrder: boolean) =>
      this.order && this.calculatePayInfo(this.order, isSingleOrder));

    this.order$ = this.paymentService.getOrderDetails(orderId)
      .pipe(
        tap((order: OrderModel) => {
          if (order?.order_id) {
            this.order = order;
            this.orderItems = order.orderItems;
            this.calculatePayInfo(order, this.paymentTypeCtrl.value);
          } else {
            this.handleLoadOrderError();
          }
        }),
      );
  }

  private calculatePayInfo(order: OrderModel, isSubscription: boolean) {
    const paymentRequest =
      isSubscription
        ? this.paymentService.getSubscriptionPaymentRequest(
          order,
          isSubscription ? 'single' : 'sub',
          PaymentCurrencyEnum.UAH,
          isSubscription ? PaymentActionEnum.Pay : PaymentActionEnum.Subscribe)
        : this.paymentService.getSinglePaymentRequest(
          order,
          isSubscription ? 'single' : 'sub');
    this.paymentData = this.paymentService.encodeBase64(JSON.stringify(paymentRequest));
    this.signature = this.paymentService.getSignature(this.paymentService.getSignString(this.paymentData));
  }

  private handleLoadOrderError(): void {
    void this.router.navigate([RoutePath.Root]);
  }
}
