import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CheckoutComponent } from 'src/app/feature/checkout/components';



@NgModule({
  declarations: [
    CheckoutComponent,
  ],
  imports: [
    CoreModule,
    SharedModule,
  ]
})
export class CheckoutModule { }
