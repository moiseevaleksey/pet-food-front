import { NgModule } from '@angular/core';
import { HomePageComponent } from './components';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';


@NgModule({
  declarations: [
    HomePageComponent,
  ],
    imports: [
        SharedModule,
        CoreModule,
    ],
})
export class HomeModule {
}
