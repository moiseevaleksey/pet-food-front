import { ShopItemFoodTypeModel } from './shop';
import { PetCategoryModel } from 'src/app/core/models';

export interface ShopFilterModel {
  petCategory: PetCategoryModel,
  brands: string[];
  foodType: ShopItemFoodTypeModel[];
  age: string[];
  petSize: string[];
  minPrice: number | null;
  maxPrice: number | null;
}

export interface ShopItemsFilterModel {
  brands: ShopItemFilterItemModel<string>[];
  foodType?: ShopItemFilterItemModel<ShopItemFoodTypeModel>[];
  age: string[];
  animalSize: string[];
  minPrice: number;
  maxPrice: number;
}

export interface ShopItemFilterItemModel<T> {
  label: string;
  value: T;
}
