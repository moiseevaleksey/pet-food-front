import { animalCategories, PaymentCurrencyEnum, ShopItemDataModel } from 'src/app/core/models';

export const DEFAULT_PET_CATEGORY = animalCategories[0];

export interface ShopItemViewModel {
  id: number;
  title: string;
  subtitle: string;
  description: string;
  img: string;
  price: string;
  currency: PaymentCurrencyEnum;
  dataItem: ShopItemDataModel;
}

export interface ShopItemFoodTypeModel {
  id: number;
  type: string;
  code: number;
}

export interface ShopItemAnimalSizeModel {
  id: number;
  shortName: string;
  longName: string;
}
