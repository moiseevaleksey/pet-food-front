import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaymentCurrencyEnum, ShopItemDataModel } from 'src/app/core/models';
import { ApiUrl, HOST } from 'src/config/api-url';
import { ApiService } from 'src/app/core/services';
import { ShopFilterModel, ShopItemFoodTypeModel, ShopItemViewModel } from 'src/app/feature/shop/models';
import { intersection } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog/dialog-ref';
import { ShopAmountSelectionComponent } from 'src/app/feature/shop/components';

@Injectable({
  providedIn: 'root',
})
export class ShopService {

  constructor(
    private readonly apiService: ApiService,
    private readonly dialog: MatDialog,
  ) {
  }

  public getShopItemsByCategory(petCategoryId: string): Observable<ShopItemDataModel[]> {
    return this.apiService.get<ShopItemDataModel[]>(`${ ApiUrl.catalogByPetCategory }/${ petCategoryId }`);
  }

  public getShopItemDetails(id: string): Observable<ShopItemDataModel> {
    return this.apiService.get<ShopItemDataModel>(`${ ApiUrl.shopItemDetails }/${ id }`);
  }

  public mapShopItemToViewModel(dataItem: ShopItemDataModel): ShopItemViewModel {
    return {
      id: dataItem.id,
      title: dataItem.skuItem.brand,
      subtitle: dataItem.skuItem.animalCategory.name,
      img: `${ HOST }/img/${ dataItem.skuItem.animalCategory.id }/small/${ dataItem.skuItem.imgName }`,
      description: dataItem.skuItem.description,
      price: String(dataItem.sellingPrice),
      currency: PaymentCurrencyEnum.UAH,
      dataItem,
    };
  }

  public filterShopItems(items: ShopItemDataModel[], filter: ShopFilterModel): ShopItemDataModel[] {
    return items
      .filter((item: ShopItemDataModel) => (
          ShopService.checkBrand(item, filter.brands) &&
          ShopService.checkAge(item, filter.age) &&
          ShopService.checkFoodType(item, filter.foodType) &&
          ShopService.checkAnimalSize(item, filter.petSize) &&
          ShopService.checkMinPrice(item, filter.minPrice) &&
          ShopService.checkMaxPrice(item, filter.maxPrice)
        ),
      );
  }

  public openAmountDialog(): MatDialogRef<ShopAmountSelectionComponent, number> {
    return this.dialog.open(ShopAmountSelectionComponent);
  }

  private static checkBrand(item: ShopItemDataModel, brands: string[] | null): boolean {
    return brands ? brands.includes(item.skuItem.brand) : false;
  }

  private static checkAge(item: ShopItemDataModel, age: string[] | null): boolean {
    return !!intersection(item.skuItem.petAgeGroups.map(itm => itm.petAgeRange), age).length;
  }

  private static checkFoodType(item: ShopItemDataModel, foodType: ShopItemFoodTypeModel[] | null): boolean {
    return item.skuItem.foodType && !!foodType && foodType.map(itm => itm.type).includes(item.skuItem.foodType.type);
  }

  private static checkAnimalSize(item: ShopItemDataModel, petSize: string[] | null): boolean {
    return !!intersection(item.skuItem.animalSizes.map(itm => itm.longName), petSize).length;
  }

  private static checkMinPrice(item: ShopItemDataModel, price: number | null): boolean {
    if (price === null) {
      return true;
    }
    return item.sellingPrice >= price;
  }

  private static checkMaxPrice(item: ShopItemDataModel, price: number | null): boolean {
    if (price === null) {
      return true;
    }
    return item.sellingPrice <= price;
  }
}
