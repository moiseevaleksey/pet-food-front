import { Pipe, PipeTransform } from '@angular/core';
import { ShopFilterModel } from '../models';
import { ShopService } from '../services';
import { ShopItemDataModel } from 'src/app/core/models';

@Pipe({
  name: 'shopItemFilter',
})
export class ShopItemFilterPipe implements PipeTransform {
  constructor(
    private shopService: ShopService,
  ) { }

  public transform(items: ShopItemDataModel[] | null, filter: ShopFilterModel | null): ShopItemDataModel[] | null {
    if (items === null) return null;
    if (filter === null) return items;

    return this.shopService.filterShopItems(items, filter);
  }

}
