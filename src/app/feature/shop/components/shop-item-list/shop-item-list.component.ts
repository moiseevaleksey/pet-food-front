import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ShopItemViewModel } from '../../models';
import { ShopService } from 'src/app/feature/shop/services';
import { ShopItemDataModel } from 'src/app/core/models';

@Component({
  selector: 'pf-item-list',
  templateUrl: './shop-item-list.component.html',
  styleUrls: ['./shop-item-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShopItemListComponent implements OnChanges {
  @Input() items: ShopItemDataModel[] | null = [];
  @Input() startIndex: number = 0;
  @Input() endIndex: number = 9;
  public mappedItems: ShopItemViewModel[] = [];

  constructor(
    private readonly shopService: ShopService,
  ) {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (!!changes?.items?.currentValue) {
      this.mappedItems = (changes.items.currentValue as ShopItemDataModel[])
        .map((item: ShopItemDataModel) => this.shopService.mapShopItemToViewModel(item));
    }
  }
}
