import { Component, OnInit } from '@angular/core';
import { BasketService, BasketType } from 'src/app/core/services';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'pf-shop-top-toolbar',
  templateUrl: './shop-top-toolbar.component.html',
  styleUrls: ['./shop-top-toolbar.component.scss'],
})
export class ShopTopToolbarComponent implements OnInit {
  public basket$: BehaviorSubject<BasketType[]> = new BehaviorSubject<BasketType[]>([]);
  public showBasketBadge$: Observable<boolean> = new Observable<boolean>();

  constructor(
    private readonly basketService: BasketService,
  ) {
  }

  public ngOnInit(): void {
    this.basket$ = this.basketService.getBasket();
    this.showBasketBadge$ = this.basket$
      .pipe(
        map((basket: BasketType[]) => !basket.length),
      )
  }


}
