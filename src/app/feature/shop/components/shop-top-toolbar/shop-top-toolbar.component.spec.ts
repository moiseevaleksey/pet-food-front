import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopTopToolbarComponent } from './shop-top-toolbar.component';

describe('ShopTopToolbarComponent', () => {
  let component: ShopTopToolbarComponent;
  let fixture: ComponentFixture<ShopTopToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopTopToolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopTopToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
