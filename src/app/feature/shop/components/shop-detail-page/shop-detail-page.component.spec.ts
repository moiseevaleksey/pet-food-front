import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopDetailPageComponent } from './shop-detail-page.component';

describe('ShopDetailPageComponent', () => {
  let component: ShopDetailPageComponent;
  let fixture: ComponentFixture<ShopDetailPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopDetailPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
