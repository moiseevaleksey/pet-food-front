import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { ShopService } from 'src/app/feature/shop/services';
import { filter, switchMap, tap } from 'rxjs/operators';
import { ShopItemDataModel } from 'src/app/core/models';
import { BasketService } from 'src/app/core/services';
import { MatDialogRef } from '@angular/material/dialog/dialog-ref';
import { ShopAmountSelectionComponent } from '../shop-amount-selection/shop-amount-selection.component';

@Component({
  selector: 'pf-shop-detail-page',
  templateUrl: './shop-detail-page.component.html',
  styleUrls: ['./shop-detail-page.component.scss'],
})
export class ShopDetailPageComponent implements OnInit, OnDestroy {
  public shopItem: ShopItemDataModel | null = null;
  public isLoading: boolean = false;
  private routeSub?: Subscription;
  private dialogRef?: MatDialogRef<ShopAmountSelectionComponent, number>;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly shopService: ShopService,
    private readonly basketService: BasketService,
  ) {
  }

  public ngOnInit(): void {
    this.route.params
      .pipe(
        tap(() => this.isLoading = true),
        switchMap((params: Params) => this.shopService.getShopItemDetails(params.id)),
      )
      .subscribe((item: ShopItemDataModel) => {
        this.shopItem = item;
        this.isLoading = false;
      });
  }

  public ngOnDestroy(): void {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

  public add(): void {
    if (!this.shopItem) return;

    this.dialogRef = this.shopService.openAmountDialog();
    this.dialogRef.afterClosed()
      .pipe(
        filter(Boolean),
      )
      .subscribe(() => {
        this.basketService.addToBasket(this.shopItem!);
      });
  }
}
