import { Component, OnInit } from '@angular/core';
import {
  ShopItemFilterItemModel,
  ShopFilterModel,
  ShopItemFoodTypeModel,
  ShopItemsFilterModel,
} from '../../models';
import { finalize, shareReplay, tap } from 'rxjs/operators';
import { ShopService } from '../../services';
import { animalCategories, PetCategoryModel, ShopItemDataModel } from 'src/app/core/models';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'pf-shop-page',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.scss'],
})
export class ShopPageComponent implements OnInit {
  public isLoading: boolean = false;
  public shopItems$: Observable<ShopItemDataModel[]> = new Observable<ShopItemDataModel[]>();
  public filterOptions: ShopItemsFilterModel | null = null;
  public filter: ShopFilterModel | null = null;
  public startIndex: number = 0;
  public endIndex: number = 9;
  public pageSize: number = 12;
  public pageSizeOptions: number[] = [12, 24, 48];

  constructor(
    private readonly shopService: ShopService,
    private readonly router: ActivatedRoute,
  ) {
  }

  public ngOnInit(): void {
    const petCategoryValue = this.router.snapshot.queryParamMap.get('petCategory');
    const petCategory = animalCategories
      .find((category: PetCategoryModel) => category.value === petCategoryValue) || animalCategories[0];
    this.loadShopItems(petCategory);
  }

  public handleNewFilter(filter: ShopFilterModel): void {
    this.filter = filter;
  }

  public handlePageSelection(event: PageEvent): void {
    this.pageSize = event.pageSize;
    this.startIndex = event.pageIndex * event.pageSize;
    this.endIndex = (event.pageIndex + 1) * event.pageSize;
  }

  private loadShopItems(petCategory: PetCategoryModel): void {
    this.isLoading = true;
    this.shopItems$ = this.shopService.getShopItemsByCategory(String(petCategory.id))
      .pipe(
        tap((shopItems: ShopItemDataModel[]) => this.filterOptions = this.getFilterOptions(shopItems)),
        finalize(() => this.isLoading = false),
        shareReplay(),
      );
  }

  private getFilterOptions(shopItems: ShopItemDataModel[]): ShopItemsFilterModel {
    const emptyFilerOptions: ShopItemsFilterModel = {
      brands: [],
      age: [],
      foodType: [],
      animalSize: [],
      minPrice: 0,
      maxPrice: 0,
    };

    return shopItems.reduce((acc: ShopItemsFilterModel, item: ShopItemDataModel): ShopItemsFilterModel => {
        const brands: ShopItemFilterItemModel<string>[] = !acc.brands.find(accItem => accItem.label === item.skuItem.brand)
          ? [...acc.brands, { label: item.skuItem.brand, value: item.skuItem.brand }]
          : acc.brands;
        const age: string[] = [...new Set([
          ...acc.age,
          ...item.skuItem.petAgeGroups.map((item) => item.petAgeRange),
        ])];
        let foodType: ShopItemFilterItemModel<ShopItemFoodTypeModel>[] = [];
        if (item.skuItem.foodType && acc.foodType) {
          foodType = !acc.foodType.find((accItem) => item.skuItem.foodType && accItem.value.id === item.skuItem.foodType.id)
            ? [...acc.foodType, { label: item.skuItem.foodType.type, value: item.skuItem.foodType }]
            : acc.foodType;
        }
        const animalSize: string[] = [...new Set([
          ...acc.animalSize,
          ...item.skuItem.animalSizes.map((item) => item.longName),
        ])];
        const minPrice = Math.min(acc.minPrice, item.sellingPrice);
        const maxPrice = Math.max(acc.maxPrice, item.sellingPrice);

        return { brands, age, foodType, animalSize, minPrice, maxPrice };
      },
      emptyFilerOptions);
  }
}
