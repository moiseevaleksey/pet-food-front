import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

const DEFAUL_AMOUNT = 1;

@Component({
  selector: 'pf-shop-amount-selection',
  templateUrl: './shop-amount-selection.component.html',
  styleUrls: ['./shop-amount-selection.component.scss'],
})
export class ShopAmountSelectionComponent implements OnInit {
  public amountControl: FormControl = new FormControl();

  constructor(
    private readonly fb: FormBuilder,
    private readonly dialogRef: MatDialogRef<ShopAmountSelectionComponent>,
  ) {
  }

  public ngOnInit(): void {
    this.amountControl = this.fb.control(DEFAUL_AMOUNT, Validators.min(1));
  }

  public submit(): void {
    this.dialogRef.close(Number(this.amountControl.value));
  }
}
