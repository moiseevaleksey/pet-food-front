import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopAmountSelectionComponent } from './shop-amount-selection.component';

describe('ShopAmountSelectionComponent', () => {
  let component: ShopAmountSelectionComponent;
  let fixture: ComponentFixture<ShopAmountSelectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopAmountSelectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopAmountSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
