import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ShopItemViewModel } from 'src/app/feature/shop/models';
import { BasketService } from 'src/app/core/services';
import { ShopService } from 'src/app/feature/shop/services';
import { MatDialogRef } from '@angular/material/dialog/dialog-ref';
import { filter } from 'rxjs/operators';
import { ShopAmountSelectionComponent } from '../shop-amount-selection/shop-amount-selection.component';

@Component({
  selector: 'pf-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss'],
})
export class ShopItemComponent {
  @Input() item: ShopItemViewModel | null = null;

  @Output() addToBasket: EventEmitter<number> = new EventEmitter<number>();

  private dialogRef?: MatDialogRef<ShopAmountSelectionComponent, number>;

  constructor(
    private readonly basketService: BasketService,
    private readonly shopService: ShopService,
  ) {
  }

  public add(): void {
    if (!this.item?.dataItem) return;

    this.dialogRef = this.shopService.openAmountDialog();
    this.dialogRef.afterClosed()
      .pipe(
        filter(Boolean),
      )
      .subscribe(() => {
        this.basketService.addToBasket(this.item?.dataItem!);
      });
  }
}
