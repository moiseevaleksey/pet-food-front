import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, map, takeUntil } from 'rxjs/operators';
import APP_CONST from 'src/config/constants';
import { Subject } from 'rxjs';
import {
  ShopItemFilterItemModel,
  ShopFilterModel,
  ShopItemFoodTypeModel,
  ShopItemsFilterModel, DEFAULT_PET_CATEGORY,
} from '../../models';

@Component({
  selector: 'pf-filters',
  templateUrl: './shop-filters.component.html',
  styleUrls: ['./shop-filters.component.scss'],
})
export class ShopFiltersComponent implements OnInit, OnDestroy, OnChanges {
  @Output() filter: EventEmitter<ShopFilterModel> = new EventEmitter<ShopFilterModel>();

  @Input() filterOptions: ShopItemsFilterModel | null = null;

  public form: FormGroup = new FormGroup({});
  public brandsFilterOptions: ShopItemFilterItemModel<string>[] = [];
  public tasteFilterOptions: ShopItemFilterItemModel<string>[] = [];
  public ageFilterOptions: string[] = [];
  public foodTypeFilterOptions: ShopItemFilterItemModel<ShopItemFoodTypeModel>[] = [];
  public petSizeFilterOptions: string[] = [];

  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private readonly fb: FormBuilder,
  ) {
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.filterOptions.currentValue) {
      const {
        brands = [],
        age = [],
        foodType = [],
        animalSize = [],
      }: ShopItemsFilterModel = changes.filterOptions.currentValue;

      this.brandsFilterOptions = brands;
      this.ageFilterOptions = age;
      this.foodTypeFilterOptions = foodType;
      this.petSizeFilterOptions = animalSize;

      const selectedBrands = brands.map(itm => itm.value);
      const selectedFoodType = foodType.map(itm => itm.value);
      this.form.patchValue({ brands: selectedBrands, age, foodType: selectedFoodType, petSize: animalSize });
    }
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  public ngOnInit(): void {
    this.form = this.fb.group({
      brands: [],
      taste: [],
      age: [],
      foodType: [],
      itemWeight: [],
      petSize: [],
      minPrice: APP_CONST.EMPTY_LINE,
      maxPrice: APP_CONST.EMPTY_LINE,
    });
    this.form.valueChanges
      .pipe(
        map((value): ShopFilterModel => {
          return {
            petCategory: DEFAULT_PET_CATEGORY,
            brands: value.brands.length ? value.brands : null,
            age: value.age.length ? value.age : null,
            foodType: value.foodType.length ? value.foodType : null,
            petSize: value.petSize.length ? value.petSize : null,
            minPrice: value.minPrice === APP_CONST.EMPTY_LINE ? null : Number(value.minPrice),
            maxPrice: value.maxPrice === APP_CONST.EMPTY_LINE ? null : Number(value.maxPrice),
          };
        }),
        takeUntil(this.destroy$),
        debounceTime(APP_CONST.DEBOUNCE_TIME),
      )
      .subscribe((filter: ShopFilterModel) => this.filter.emit(filter));
  }
}
