import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopFiltersComponent } from 'src/app/feature/shop/components/shop-filters/shop-filters.component';

describe('FiltersComponent', () => {
  let component: ShopFiltersComponent;
  let fixture: ComponentFixture<ShopFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopFiltersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
