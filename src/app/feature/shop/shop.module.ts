import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/core/core.module';
import {
  ShopAmountSelectionComponent,
  ShopBasketComponent,
  ShopDetailPageComponent,
  ShopFiltersComponent,
  ShopItemComponent,
  ShopItemListComponent,
  ShopPageComponent,
  ShopTopToolbarComponent,
} from './components';
import { SharedModule } from 'src/app/shared/shared.module';
import { ShopItemFilterPipe } from './pipe/shop-item-filter.pipe';


@NgModule({
  declarations: [
    ShopBasketComponent,
    ShopDetailPageComponent,
    ShopFiltersComponent,
    ShopItemComponent,
    ShopItemListComponent,
    ShopPageComponent,
    ShopTopToolbarComponent,
    ShopItemFilterPipe,
    ShopAmountSelectionComponent,
  ],
  imports: [
    CoreModule,
    SharedModule,
  ],
  exports: [
    ShopTopToolbarComponent,
  ],
})
export class ShopModule {
}
