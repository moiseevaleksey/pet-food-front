import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/core/core.module';
import {
  DogSizeStepComponent,
  FrequencyInfoStepComponent,
  GeneralInfoStepComponent,
  OtherInfoStepComponent,
  PetTypeStepComponent,
  SuggestedFoodOptionsComponent,
} from './components';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import APP_CONST from 'src/config/constants';
import { SurveyRoutesEnum } from 'src/app/feature/survey/survey.config';
import { SurveyPageComponent } from './components/survey-page/survey-page.component';
// import RoutePath from 'src/config/routes';


const routes: Routes = [
  {
    path: APP_CONST.EMPTY_LINE,
    redirectTo: SurveyRoutesEnum.PetType,
    pathMatch: 'full',
  },
  {
    path: APP_CONST.EMPTY_LINE,
    component: SurveyPageComponent,
    children: [
      {
        path: SurveyRoutesEnum.PetType,
        component: PetTypeStepComponent,
        pathMatch: 'full',
      },
      {
        path: SurveyRoutesEnum.MainInfo,
        component: GeneralInfoStepComponent,
        pathMatch: 'full',
        // canActivate: [SurveyGuard],
      },
      {
        path: SurveyRoutesEnum.DogSize,
        component: DogSizeStepComponent,
        // canActivate: [SurveyGuard],
        pathMatch: 'full',
      },
      {
        path: SurveyRoutesEnum.OtherInfo,
        component: OtherInfoStepComponent,
        // canActivate: [SurveyGuard],
      },
      {
        path: SurveyRoutesEnum.Suggested,
        component: SuggestedFoodOptionsComponent,
        // canActivate: [SurveyGuard],
      },
      {
        path: '**',
        redirectTo: APP_CONST.EMPTY_LINE,
      }
    ],
  },
];

@NgModule({
  declarations: [
    PetTypeStepComponent,
    GeneralInfoStepComponent,
    OtherInfoStepComponent,
    DogSizeStepComponent,
    FrequencyInfoStepComponent,
    SuggestedFoodOptionsComponent,
    SurveyPageComponent,
  ],
  imports: [
    CoreModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class SurveyModule {
}
