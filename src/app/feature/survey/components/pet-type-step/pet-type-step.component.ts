import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {SurveyService} from '../../services';
import {animalCategories} from 'src/app/core/models/config';
import {PetCategoryModel} from 'src/app/core/models';
import {Router} from '@angular/router';
import RoutePath from 'src/config/routes';
import {SurveyStepEnum} from 'src/app/feature/survey/models/survey.model';
import {SurveyRoutesEnum} from 'src/app/feature/survey/survey.config';
import APP_CONST from "../../../../../config/constants";

@Component({
  selector: 'pf-pet-type-step',
  templateUrl: './pet-type-step.component.html',
  styleUrls: ['./pet-type-step.component.scss'],
})
export class PetTypeStepComponent implements OnInit {
  public petType: FormControl = new FormControl();
  public types: PetCategoryModel[] = [];

  constructor(
    private readonly fb: FormBuilder,
    private readonly surveyService: SurveyService,
    private readonly router: Router,
  ) {
  }

  public ngOnInit(): void {
    const state = this.surveyService.getSurveyState();
    const petCategory = state[SurveyStepEnum.PetType];

    this.petType = this.fb.control(petCategory || APP_CONST.EMPTY_LINE, Validators.required);
    this.types = animalCategories;
  }

  public handlePetTypeSelection(): void {
    const petCategory: PetCategoryModel = this.petType.value;
    this.surveyService.setPetType(petCategory);
    debugger;
    petCategory.main
      ? void this.router.navigate([RoutePath.Survey, SurveyRoutesEnum.MainInfo])
      : void this.router.navigate([RoutePath.Shop], {queryParams: {petCategory: petCategory?.value}})
  }

  public handleBack(): void {
    this.router.navigate([RoutePath.Root]).then(() => this.surveyService.resetSurveyState());
  }
}
