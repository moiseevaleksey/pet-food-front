import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetTypeStepComponent } from './pet-type-step.component';

describe('PetTypeStepComponent', () => {
  let component: PetTypeStepComponent;
  let fixture: ComponentFixture<PetTypeStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetTypeStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetTypeStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
