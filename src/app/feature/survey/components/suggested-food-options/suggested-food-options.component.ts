import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SurveyService } from 'src/app/feature/survey/services';
import { from, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService, AuthService, BasketService, PaymentService } from 'src/app/core/services';
import { BoxLineItemModel, MealBoxModel, RecommendedBoxesModel, SurveyStepEnum } from '../../models/survey.model';
import { filter, finalize, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import RoutePath from 'src/config/routes';
import { PetModel } from 'src/app/core/models';
import { getAgeInMonth } from 'src/app/shared/utils/utils';
import { SurveyRoutesEnum } from 'src/app/feature/survey/survey.config';

interface BoxViewModel {
  brand: string;
  totalPrice: number;
  totalWeight: number;
  details: BoxDetailsModel[];
  lineItems: BoxLineItemModel[];
  box: MealBoxModel;
}

interface BoxDetailsModel {
  taste: string;
  foodType: string;
  itemCount: number;
  itemWeight: number;
}

@Component({
  selector: 'pf-suggested-food-options',
  templateUrl: './suggested-food-options.component.html',
  styleUrls: ['./suggested-food-options.component.scss'],
})
export class SuggestedFoodOptionsComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public mealBoxes$: Observable<MealBoxModel[]> = new Observable<MealBoxModel[]>();
  public viewMealBoxes$: Observable<BoxViewModel[]> = new Observable<BoxViewModel[]>();
  public isLoading: boolean = false;

  constructor(
    private readonly fb: FormBuilder,
    private readonly surveyService: SurveyService,
    private readonly paymentService: PaymentService,
    private readonly router: Router,
    private readonly api: ApiService,
    private readonly auth: AuthService,
    private readonly basket: BasketService,
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      meal: [null, Validators.required],
    })
    this.loadSuggestedFood();
  }

  public setMealOption(): void {
    const { meal }: { meal: BoxViewModel } = this.form.getRawValue();
    const state = this.surveyService.getSurveyState();
    const petCategory = state[SurveyStepEnum.PetType];
    const mainInfo = state[SurveyStepEnum.MainInfo];
    const dogSize = state[SurveyStepEnum.DogSize];
    const name = mainInfo?.name;
    const age = getAgeInMonth(mainInfo?.months!, mainInfo?.years!);
    const petCategoryId = petCategory?.id || null;
    const orderSkuItems = meal.lineItems.reduce((acc: any[], item: BoxLineItemModel) => [...acc, {
      skuItemId: item.lineItem.id,
      quantity: item.count,
    }], [])

    this.surveyService.createAnimal({ petCategoryId, name, age, adultDogSize: dogSize })
      .pipe(
        switchMap(({ id: animalId }: PetModel) =>
          this.surveyService.saveOrderToServer({ animalId, orderSkuItems })),
        tap(order => this.paymentService.setOrder(order)),
        switchMap(() => from(this.router.navigate([RoutePath.UserInfo]))),
        filter(Boolean),
      )
      .subscribe(() => {
        this.basket.addToBasket(meal.box);
        this.surveyService.resetSurveyState();
      });
  }

  public navigateToShop(): void {
    const state = this.surveyService.getSurveyState();
    const petCategory = state[SurveyStepEnum.PetType];
    this.router.navigate([RoutePath.Shop], { queryParams: { petCategory: petCategory?.value } })
      .then((isSuccess) => {
        if (isSuccess) {
          this.surveyService.resetSurveyState();
        }
      });
  }

  public handleBack(): void {
    void this.router.navigate([
      RoutePath.Survey,
      this.surveyService.getPreviousStep(SurveyRoutesEnum.Suggested),
    ]);
  }

  private loadSuggestedFood(): void {
    const mappedValue = this.surveyService.mapState(this.surveyService.getSurveyState());
    this.isLoading = true;
    this.mealBoxes$ = this.surveyService.getRecommendedFood(mappedValue)
      .pipe(
        shareReplay(),
        map((mealOptions: RecommendedBoxesModel) => mealOptions.boxes),
        finalize(() => this.isLoading = false),
      );

    this.viewMealBoxes$ = this.mealBoxes$
      .pipe(
        map((boxes: MealBoxModel[]): BoxViewModel[] => SuggestedFoodOptionsComponent.mapMealBoxToViewItem(boxes)),
      );
  }

  private static mapMealBoxToViewItem(boxes: MealBoxModel[]): BoxViewModel[] {
    return boxes.map((box: MealBoxModel) => ({
        brand: box.brand,
        totalPrice: box.totalPrice,
        totalWeight: box.lineItems.reduce((acc: number, lineItem: BoxLineItemModel) =>
            acc + (lineItem.count * lineItem.lineItem.packageWeightKilos),
          0),
        details: box.lineItems.map((item: BoxLineItemModel) => ({
            taste: item.lineItem.taste,
            foodType: item.lineItem.foodType.type,
            itemCount: item.count,
            itemWeight: item.lineItem.packageWeightKilos,
          }),
        ),
        lineItems: box.lineItems,
        box,
      }),
    );
  }
}
