import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestedFoodOptionsComponent } from './suggested-food-options.component';

describe('SuggestedFoodOptionsComponent', () => {
  let component: SuggestedFoodOptionsComponent;
  let fixture: ComponentFixture<SuggestedFoodOptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuggestedFoodOptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestedFoodOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
