import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DogSizeStepComponent } from 'src/app/feature/survey/components/dog-size-step/dog-size-step.component';

describe('GogSizeStepComponent', () => {
  let component: DogSizeStepComponent;
  let fixture: ComponentFixture<DogSizeStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DogSizeStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DogSizeStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
