import { Component, OnInit } from '@angular/core';
import { SurveyOptionModel, SurveyStepEnum } from '../../models/survey.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SurveyService } from 'src/app/feature/survey/services';
import { dogSize } from 'src/app/core/models/config';
import { DogSizeEnum } from 'src/app/core/models/core';
import RoutePath from 'src/config/routes';
import { Router } from '@angular/router';
import { SurveyRoutesEnum } from 'src/app/feature/survey/survey.config';

@Component({
  selector: 'pf-dog-size-step',
  templateUrl: './dog-size-step.component.html',
  styleUrls: ['./dog-size-step.component.scss'],
})
export class DogSizeStepComponent implements OnInit {
  public dogSizeOptions: SurveyOptionModel<DogSizeEnum>[] = dogSize;
  public form: FormGroup = new FormGroup({});

  constructor(
    private readonly fb: FormBuilder,
    private readonly surveyService: SurveyService,
    private readonly router: Router,
  ) {
  }

  ngOnInit(): void {
    const state = this.surveyService.getSurveyState();

    this.form = this.fb.group({
      dogSize: [state[SurveyStepEnum.DogSize] || null, Validators.required],
    })
  }

  public setDogSize(): void {
    this.surveyService.setDogSize(this.form.getRawValue().dogSize);
    void this.router.navigate([RoutePath.Survey, SurveyRoutesEnum.Suggested]);
  }

  public handleBack(): void {
    void this.router.navigate([RoutePath.Survey, SurveyRoutesEnum.MainInfo]);
  }

}
