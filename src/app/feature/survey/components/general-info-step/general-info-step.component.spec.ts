import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralInfoStepComponent } from 'src/app/feature/survey/components/general-info-step/general-info-step.component';

describe('DogInfoFormComponent', () => {
  let component: GeneralInfoStepComponent;
  let fixture: ComponentFixture<GeneralInfoStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralInfoStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralInfoStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
