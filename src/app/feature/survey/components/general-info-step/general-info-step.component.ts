import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SurveyService } from 'src/app/feature/survey/services';
import { FoodTypeEnum, PetTypeEnum } from 'src/app/core/models/core';
import { FoodTypeOptions } from 'src/app/core/models/config';
import { SurveyOptionModel, SurveyStepEnum } from '../../models/survey.model';
import { Router } from '@angular/router';
import RoutePath from 'src/config/routes';
import { SurveyRoutesEnum } from 'src/app/feature/survey/survey.config';

const MAX_YEARS = 25;
const MAX_MONTHS = 12;

@Component({
  selector: 'pf-general-info-step',
  templateUrl: './general-info-step.component.html',
  styleUrls: ['./general-info-step.component.scss'],
})
export class GeneralInfoStepComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public years: number[] = Array.from(Array(MAX_YEARS).keys());
  public months: number[] = Array.from(Array(MAX_MONTHS).keys());
  public foodTypes: SurveyOptionModel<FoodTypeEnum>[] = FoodTypeOptions;

  constructor(
    private readonly fb: FormBuilder,
    private readonly surveyService: SurveyService,
    private readonly router: Router,
  ) {
  }

  public ngOnInit(): void {
    const mainInfo = this.surveyService.getSurveyState()[SurveyStepEnum.MainInfo];

    this.form = this.fb.group({
      name: [mainInfo?.name || 'APP_CONST.EMPTY_LINE', Validators.required],
      years: [mainInfo?.years || 2, Validators.required],
      months: [mainInfo?.months || 2, Validators.required],
      foodType: [mainInfo?.foodType || 1, Validators.required],
    });
  }

  public setGeneralInfo(): void {
    const state = this.surveyService.getSurveyState();
    const petCategory = state[SurveyStepEnum.PetType];
    const { name, years, months, foodType } = this.form.getRawValue();

    this.surveyService.setMainInfo({ name, foodType, months, years });
    void this.router.navigate([
      RoutePath.Survey,
      petCategory?.value === PetTypeEnum.Dog ? SurveyRoutesEnum.DogSize : SurveyRoutesEnum.Suggested,
    ]);
  }

  public handleBack(): void {
    void this.router.navigate([
      RoutePath.Survey,
      this.surveyService.getPreviousStep(SurveyRoutesEnum.MainInfo),
    ]);
  }
}
