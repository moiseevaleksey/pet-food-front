import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { frequencyOptions } from 'src/app/core/models/config';
import { PurchaseFrequencyEnum } from 'src/app/core/models/core';
import { SurveyOptionModel } from '../../models/survey.model';

@Component({
  selector: 'pf-frequency-info-step',
  templateUrl: './frequency-info-step.component.html',
  styleUrls: ['./frequency-info-step.component.scss']
})
export class FrequencyInfoStepComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public frequencyOptions: SurveyOptionModel<PurchaseFrequencyEnum>[] = frequencyOptions;

  constructor(
    private readonly fb: FormBuilder,
    // private readonly surveyService: SurveyService,
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      frequency: [null, Validators.required],
    })
  }

  public setFrequency(): void {
    // this.surveyService.setPurchaseFrequency(this.form.getRawValue().frequency);
  }

}
