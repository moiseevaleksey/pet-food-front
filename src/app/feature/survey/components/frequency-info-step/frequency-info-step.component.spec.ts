import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrequencyInfoStepComponent } from './frequency-info-step.component';

describe('FrequencyInfoStepComponent', () => {
  let component: FrequencyInfoStepComponent;
  let fixture: ComponentFixture<FrequencyInfoStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrequencyInfoStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrequencyInfoStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
