import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherInfoStepComponent } from 'src/app/feature/survey/components/other-info-step/other-info-step.component';

describe('OtheInfoStepComponent', () => {
  let component: OtherInfoStepComponent;
  let fixture: ComponentFixture<OtherInfoStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherInfoStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherInfoStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
