import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SurveyService } from 'src/app/feature/survey/services';
import RoutePath from 'src/config/routes';
import { Router } from '@angular/router';
import { SurveyRoutesEnum } from 'src/app/feature/survey/survey.config';

@Component({
  selector: 'pf-other-info-step',
  templateUrl: './other-info-step.component.html',
  styleUrls: ['./other-info-step.component.scss'],
})
export class OtherInfoStepComponent implements OnInit {
  public form: FormGroup = new FormGroup({});

  constructor(
    private readonly fb: FormBuilder,
    private readonly surveyService: SurveyService,
    private readonly router: Router,
  ) {
  }

  public ngOnInit(): void {
    this.form = this.fb.group({
      name: [null, Validators.required],
      breed: [null, Validators.required],
    })
  }

  public setOtherInfo(): void {
    const otherInfo = this.form.getRawValue();
    this.surveyService.setOtherInfo(otherInfo);
    void this.router.navigate([
      RoutePath.Survey,
      this.surveyService.getPreviousStep(SurveyRoutesEnum.OtherInfo),
    ]);
  }
}
