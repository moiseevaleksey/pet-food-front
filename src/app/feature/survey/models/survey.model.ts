import { DogSizeEnum, FoodTypeEnum, FoodTypeModel, PetCategoryModel } from 'src/app/core/models/core';

export interface SurveyModel {
  [SurveyStepEnum.PetType]: PetCategoryModel | null;
  [SurveyStepEnum.MainInfo]: MainInfoModel | null;
  [SurveyStepEnum.DogSize]: DogSizeEnum | null;
  [SurveyStepEnum.OtherInfo]: OtherInfoModel | null;
  [SurveyStepEnum.Suggestion]: MealBoxModel | null;
}

export interface SurveyOptionModel<T> {
  id: number;
  value: T;
  label: string;
  ico?: string;
}

export interface MainInfoModel {
  name: string;
  years: number;
  months: number;
  foodType: FoodTypeEnum;
}

export interface OtherInfoModel {
  name: string;
  breed: string;
}

export enum SurveyStepEnum {
  PetType = 'PetType',
  MainInfo = 'MainInfo',
  OtherInfo = 'OtherInfo',
  DogSize = 'DogSize',
  Suggestion = 'Suggestion',
}

export interface MealModel {
  animalCategory: { id: number; name: string }
  brand: string;
  description: string;
  id: number;
  imgName: string;
  largeImgPath: string;
  manufacturer: any;
  originCountry: any;
  packageWeightKilos: number;
  skuName: string;
  smallImgPath: string;
  taste: string;
}

export interface SurveyResultModel {
  petCategoryId: number;
  name: string;
  subCategoryName?: string;
  age: number;
  adultDogSize: DogSizeEnum;
  preferableFoodId: FoodTypeEnum;
}

export interface RecommendedBoxesModel {
  boxes: MealBoxModel[]
}

export interface MealBoxModel {
  brand: string;
  totalPrice: number;
  lineItems: BoxLineItemModel[];
}

export interface BoxLineItemModel {
  lineItem: {
    id: number;
    originCountry?: null;
    manufacturer?: null;
    foodType: FoodTypeModel;
    brand: string;
    skuName: string;
    taste: string;
    description: string;
    animalCategory: {
      id: number;
      name: string;
    };
    packageWeightKilos: number;
    smallImgPath: string;
    largeImgPath: string;
    imgName: string;
  };
  count: number;
}
