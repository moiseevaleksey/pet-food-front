import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { SurveyService } from 'src/app/feature/survey/services';
import { SurveyRoutesEnum } from 'src/app/feature/survey/survey.config';

@Injectable({
  providedIn: 'root',
})
export class SurveyGuard implements CanActivate {
  constructor(
    private readonly surveyService: SurveyService,
    private readonly router: Router,
  ) {
  }

  public canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const url = state.url;
    const completedSteps = this.surveyService.getCompletedSteps();
    const lastInCompletedStep = this.surveyService.getLastInCompletedStep();
    const availableSteps = [...completedSteps, lastInCompletedStep];
    const isCurrentStepAvailable = !!availableSteps.find((step: SurveyRoutesEnum) => url.includes(step));

    return isCurrentStepAvailable || this.router.parseUrl(lastInCompletedStep);
  }
}
