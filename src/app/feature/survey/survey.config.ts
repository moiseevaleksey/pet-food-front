import { SurveyModel, SurveyStepEnum } from 'src/app/feature/survey/models/survey.model';
import { PetTypeEnum } from 'src/app/core/models';

export enum SurveyRoutesEnum {
  PetType = 'pet-type',
  MainInfo = 'main-info',
  DogSize = 'dog-size',
  OtherInfo = 'other-info',
  Suggested = 'suggested',
}

export const INITIAL_SURVEY_STATE: SurveyModel = {
  [SurveyStepEnum.PetType]: null,
  [SurveyStepEnum.MainInfo]: null,
  [SurveyStepEnum.DogSize]: null,
  [SurveyStepEnum.OtherInfo]: null,
  [SurveyStepEnum.Suggestion]: null,
};

export const dogSurveyStepOrder = [SurveyRoutesEnum.PetType, SurveyRoutesEnum.MainInfo, SurveyRoutesEnum.DogSize, SurveyRoutesEnum.Suggested];
export const catSurveyStepOrder = [SurveyRoutesEnum.PetType, SurveyRoutesEnum.MainInfo, SurveyRoutesEnum.Suggested];
export const otherSurveyStepOrder = [SurveyRoutesEnum.PetType, SurveyRoutesEnum.OtherInfo];
export const stepsOrderMapping: {[key in PetTypeEnum]?: SurveyRoutesEnum[]} = {
  [PetTypeEnum.Dog]: dogSurveyStepOrder,
  [PetTypeEnum.Cat]: catSurveyStepOrder,
};

export type SurveyUrlMappingType = { [key in SurveyRoutesEnum]: SurveyStepEnum };
export const surveyUrlMapping: SurveyUrlMappingType = {
  [SurveyRoutesEnum.PetType]: SurveyStepEnum.PetType,
  [SurveyRoutesEnum.MainInfo]: SurveyStepEnum.MainInfo,
  [SurveyRoutesEnum.DogSize]: SurveyStepEnum.DogSize,
  [SurveyRoutesEnum.Suggested]: SurveyStepEnum.Suggestion,
  [SurveyRoutesEnum.OtherInfo]: SurveyStepEnum.OtherInfo,
}
