import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DogSizeEnum, PetCategoryModel } from 'src/app/core/models/core';
import { ApiService, OrderModel1 } from 'src/app/core/services';
import { ApiUrl } from 'src/config/api-url';
import { getAgeInMonth } from 'src/app/shared/utils/utils';
import {
  MainInfoModel,
  OtherInfoModel,
  RecommendedBoxesModel,
  SurveyModel,
  SurveyResultModel,
  SurveyStepEnum,
} from 'src/app/feature/survey/models/survey.model';
import {
  INITIAL_SURVEY_STATE,
  otherSurveyStepOrder,
  stepsOrderMapping,
  SurveyRoutesEnum,
  surveyUrlMapping,
} from 'src/app/feature/survey/survey.config';

@Injectable({
  providedIn: 'root',
})
export class SurveyService {
  private surveyState: BehaviorSubject<SurveyModel> = new BehaviorSubject<SurveyModel>(INITIAL_SURVEY_STATE);

  constructor(
    private readonly api: ApiService,
  ) {
  }

  get petType(): PetCategoryModel | null {
    return this.surveyState.getValue()[SurveyStepEnum.PetType];
  }

  // get mainInfo(): MainInfoModel | null {
  //   return this.surveyState.getValue()[SurveyStepEnum.MainInfo];
  // }

  public getSurveyState(): SurveyModel {
    return this.surveyState.getValue();
  }

  public setPetType(petCategory: PetCategoryModel): void {
    this.surveyState.next({ ...this.surveyState.getValue(), [SurveyStepEnum.PetType]: petCategory });
  }

  public setMainInfo(mainInfo: MainInfoModel): void {
    const state = this.surveyState.getValue();
    this.surveyState.next({ ...state, [SurveyStepEnum.MainInfo]: mainInfo });
  }

  public setDogSize(dogSize: DogSizeEnum): void {
    this.surveyState.next({ ...this.surveyState.getValue(), [SurveyStepEnum.DogSize]: dogSize });
  }

  public setOtherInfo(otherInfo: OtherInfoModel) {
    this.surveyState.next({ ...this.surveyState.getValue(), [SurveyStepEnum.OtherInfo]: otherInfo });
  }

  public resetSurveyState(): void {
    this.surveyState.next({} as SurveyModel);
  }

  public getLastInCompletedStep(): SurveyRoutesEnum {
    const state = this.surveyState.getValue();
    const petType = this.petType;
    if (petType == null) return SurveyRoutesEnum.PetType;
    const currentOrder = stepsOrderMapping[petType.value] || otherSurveyStepOrder;
    return currentOrder
      .find((step: SurveyRoutesEnum) => state[surveyUrlMapping[step]] === null) || currentOrder[currentOrder.length - 1];
  }

  public getCompletedSteps(): SurveyRoutesEnum[] {
    const petType = this.petType;
    if (petType == null) return [];
    const currentOrder = stepsOrderMapping[petType.value] || otherSurveyStepOrder;
    const state = this.getSurveyState();

    return currentOrder
      .filter((step: SurveyRoutesEnum) => state[surveyUrlMapping[step]] !== null);
  }

  public getPreviousStep(currStep: SurveyRoutesEnum): SurveyRoutesEnum | null {
    const petType = this.petType;
    if (petType == null) return null;
    const currentOrder = stepsOrderMapping[petType.value] || otherSurveyStepOrder;
    const currentStepIndex = currentOrder.findIndex((step: SurveyRoutesEnum) => step === currStep);
    return currentOrder[currentStepIndex - 1];
  }

  public getRecommendedFood(body: SurveyResultModel): Observable<RecommendedBoxesModel> {
    return this.api.post<RecommendedBoxesModel>(ApiUrl.recommendedFood, body);
  }

  public mapState(state: SurveyModel): SurveyResultModel {
    return {
      petCategoryId: state[SurveyStepEnum.PetType]!.id,
      adultDogSize: state[SurveyStepEnum.DogSize]!,
      age: getAgeInMonth(state[SurveyStepEnum.MainInfo]?.years!, state[SurveyStepEnum.MainInfo]?.months!),
      name: state[SurveyStepEnum.MainInfo]!.name,
      preferableFoodId: state[SurveyStepEnum.MainInfo]!.foodType,
    }
  }

  public saveOrderToServer(order: any): Observable<OrderModel1> {
    return this.api.post<OrderModel1>(ApiUrl.saveOrder, order);
  }

  public createAnimal(animal: any): Observable<any> {
    return this.api.post(ApiUrl.createAnimal, animal);
  }
}
