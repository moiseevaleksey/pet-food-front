import { NgModule } from '@angular/core';
import {
  AdminPageComponent,
  AdminSideBarComponent,
  AdminTopBarComponent,
  ClientsPageComponent,
  OrdersPageComponent,
  ProductsPageComponent,
} from './components';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import APP_CONST from 'src/config/constants';

export enum AdminRoutesEnum {
  CLIENTS = 'clients',
  ORDERS = 'orders',
  PRODUCTS = 'products',
}

const routes: Routes = [
  {
    path: APP_CONST.EMPTY_LINE,
    component: AdminPageComponent,
    children: [
      {
        path: AdminRoutesEnum.CLIENTS,
        component: ClientsPageComponent,
      },
      {
        path: AdminRoutesEnum.ORDERS,
        component: OrdersPageComponent,
      },
      {
        path: AdminRoutesEnum.PRODUCTS,
        component: ProductsPageComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    AdminPageComponent,
    AdminSideBarComponent,
    AdminTopBarComponent,
    ClientsPageComponent,
    OrdersPageComponent,
    ProductsPageComponent,
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class AdminModule {
}
