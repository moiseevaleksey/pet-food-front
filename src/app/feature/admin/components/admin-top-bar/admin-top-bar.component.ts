import { Component, EventEmitter, Output } from '@angular/core';
import { IS_SIDEBAR_OPEN_BY_DEFAULT } from '../admin-page/admin-page.component';

@Component({
  selector: 'pf-admin-top-bar',
  templateUrl: './admin-top-bar.component.html',
  styleUrls: ['./admin-top-bar.component.scss']
})
export class AdminTopBarComponent {
  @Output() public sideMenuState: EventEmitter<boolean> = new EventEmitter<boolean>();
  public isSideMenuOpened: boolean = IS_SIDEBAR_OPEN_BY_DEFAULT;

  constructor() { }

  public toggleSizeMenu(isSideMenuOpened: boolean): void {
    this.isSideMenuOpened = !isSideMenuOpened;
    this.sideMenuState.emit(this.isSideMenuOpened);
  }

}
