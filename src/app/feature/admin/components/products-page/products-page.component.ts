import { Component } from '@angular/core';

@Component({
  selector: 'pf-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent {

  constructor() { }
}
