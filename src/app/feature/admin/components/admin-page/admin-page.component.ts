import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

export const IS_SIDEBAR_OPEN_BY_DEFAULT = true;

@Component({
  selector: 'pf-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss'],
})
export class AdminPageComponent implements OnInit {
  public isSideMenuOpened: boolean = IS_SIDEBAR_OPEN_BY_DEFAULT;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
  ) {
  }

  public ngOnInit(): void {
    this.activatedRoute.url.subscribe(console.log)
  }

  public handleSidebarStateChange(state: boolean): void {
    this.isSideMenuOpened = state;
  }

}
