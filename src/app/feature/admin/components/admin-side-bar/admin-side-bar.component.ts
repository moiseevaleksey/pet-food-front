import { Component } from '@angular/core';
import { AdminRoutesEnum } from 'src/app/feature/admin/admin.module';

@Component({
  selector: 'pf-admin-side-bar',
  templateUrl: './admin-side-bar.component.html',
  styleUrls: ['./admin-side-bar.component.scss'],
})
export class AdminSideBarComponent {
  public routesEnum: typeof AdminRoutesEnum = AdminRoutesEnum;

  constructor() {
  }

}
