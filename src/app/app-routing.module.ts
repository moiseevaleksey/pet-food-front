import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import RoutePath from 'src/config/routes';
import { ShopDetailPageComponent, ShopPageComponent } from 'src/app/feature/shop/components';
import {
  BasketComponent,
  PurchaseSuccessComponent,
  UserInfoFormComponent,
} from 'src/app/shared/components';
import { HomePageComponent } from 'src/app/feature/home/components';
import { CheckoutComponent } from 'src/app/feature/checkout/components';

const routes: Routes = [
  {
    path: RoutePath.Root,
    component: HomePageComponent,
  },
  {
    path: RoutePath.Survey,
    loadChildren: () => import('src/app/feature/survey/survey.module').then(m => m.SurveyModule),
  }, {
    path: RoutePath.Shop,
    component: ShopPageComponent,
  }, {
    path: RoutePath.Shop + '/:id',
    component: ShopDetailPageComponent,
  }, {
    path: RoutePath.Checkout,
    component: CheckoutComponent,
  }, {
    path: RoutePath.UserInfo,
    component: UserInfoFormComponent,
  }, {
    path: RoutePath.PurchaseSuccess,
    component: PurchaseSuccessComponent,
  }, {
    path: RoutePath.Basket,
    component: BasketComponent,
  }, {
    path: RoutePath.Login,
    loadChildren: () => import('src/app/feature/login/login.module').then(m => m.LoginModule),
  }, {
    path: RoutePath.Admin,
    loadChildren: () => import('src/app/feature/admin/admin.module').then(m => m.AdminModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
